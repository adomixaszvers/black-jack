#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include "shuffle.h"

#ifndef GAME_H
#define GAME_H

void game_loop(const int client1fd, const int client2fd);
void test_player_stats(void);

#endif
