#include <stdlib.h>
#include <stdio.h>

/* Arrange the N elements of ARRAY in random order.
   Only effective if N is much smaller than RAND_MAX;
   if this may not be the case, use a better random
   number generator. */
#ifndef SHUFFLE_H
#define SHUFFLE_H

void shuffle(int *array, size_t n);
void randomize(void);

#endif
