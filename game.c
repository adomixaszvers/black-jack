#include "game.h"
#define NUMBER_OF_CARDS 52
#define MAX_IN_A_HAND 11
#define MESSAGE_LEN 512

struct player {
    int aces;
    int other_cards[MAX_IN_A_HAND];
    int number_of_cards;
    int score;
    int stays;
};

void print_card(FILE *buff_stream, int number);
void final_message(struct player *player1, struct player *player2, void *buff, size_t buff_size);
int draw_card(int deck[], int *used_cards, struct player *some_player);
void players_stats(struct player *player1, struct player *player2, void *buff, size_t buff_size);
int calculate_sum(struct player *some_player);


void game_loop(const int client1fd, const int client2fd) {
    int sendrecv_error=0;
    int deck[NUMBER_OF_CARDS];
    int i;
    char message[MESSAGE_LEN];
    int used_cards=0;
    struct player client1, client2;
    int client1sum, client2sum;

    client1.aces = 0;
    client1.number_of_cards=0;
    client1.stays=0;
    client1.score=0;

    client2.aces = 0;
    client2.number_of_cards=0;
    client2.stays=0;
    client2.score=0;

    for(i=0; i<NUMBER_OF_CARDS; i++) {
        deck[i] = i;
    }
    randomize();
    shuffle(deck, NUMBER_OF_CARDS);

    for (;;) {

        final_message(&client1, &client2, (void *) message, MESSAGE_LEN);
        if(send(client1fd, (void *) message, strlen(message), 0) < 0) {
            fprintf(stderr, "Cannot send message for client1.\n");
	    sendrecv_error = 1;
        }
	if(recv(client1fd, (void *) message, MESSAGE_LEN, 0) <=0) {
                fprintf(stderr, "Cannot receive message from client1\n");
                sendrecv_error = 1;
            }
        final_message(&client2, &client1, (void *) message, MESSAGE_LEN);
        if(send(client2fd, (void *) message, strlen(message), 0) < 0) {
            fprintf(stderr, "Cannot send message for client2.\n");
	    sendrecv_error = 1;
        }
	if(recv(client2fd, (void *) message, MESSAGE_LEN, 0) <=0) {
                fprintf(stderr, "Cannot receive message from client1\n");
                sendrecv_error = 1;
            }
        if(used_cards >= NUMBER_OF_CARDS - 4 || sendrecv_error) { /*  */
            return;
        }
        client1.stays = 0;
        client1.aces = 0;
        client1.number_of_cards = 0;
        client2.stays = 0;
        client2.aces = 0;
        client2.number_of_cards = 0;

        draw_card(deck, &used_cards, &client1);
        draw_card(deck, &used_cards, &client2);
        draw_card(deck, &used_cards, &client1);
        draw_card(deck, &used_cards, &client2);

        while(!(client1.stays && client2.stays)) {
            players_stats(&client1, &client2, (void *) message, MESSAGE_LEN);
            if(send(client1fd, (void *) message, strlen(message), 0) < 0) {
                fprintf(stderr, "Cannot send message for client1.\n");
                return;
            }
            if(recv(client1fd, (void *) message, MESSAGE_LEN, 0) <=0) {
                fprintf(stderr, "Cannot receive message from client1\n");
                return;
            }
            if(message[0] == 'S') {
                client1.stays = 1;
            }
            if (!client1.stays) {
                if(draw_card(deck, &used_cards, &client1) < 0)
                    break;
            }
            if(calculate_sum(&client1) > 21) {
                client1.stays = 1;
            }


            players_stats(&client2, &client1, (void *) message, MESSAGE_LEN);
            if(send(client2fd, (void *) message, strlen(message), 0) < 0) {
                fprintf(stderr, "Cannot send message for client2.\n");
                return;
            }
            if(recv(client2fd, (void *) message, MESSAGE_LEN, 0) <=0) {
                fprintf(stderr, "Cannot receive message from client2\n");
                return;
            }
            if(message[0] == 'S') {
                client2.stays = 1;
            }
            if(!client2.stays) {
                if(draw_card(deck, &used_cards, &client2) < 0)
                    break;
            }

            if(calculate_sum(&client2) > 21) {
                client2.stays = 1;
            }

        }
        client1sum = calculate_sum(&client1);
        client2sum = calculate_sum(&client2);

        if((client1sum <= 21) && (client1sum > client2sum || client2sum > 21))
            client1.score += client1.number_of_cards;

        if((client2sum <= 21) && (client2sum > client1sum || client1sum > 21))
            client2.score += client2.number_of_cards;
    }
}

void players_stats(struct player *player1, struct player *player2, void *buff, size_t buff_size) {
    int i;
    FILE *buff_stream;
    buff_stream = fmemopen(buff, buff_size, "w");
    fprintf(buff_stream, "==\nYour %d cards:\n", player1->number_of_cards);
    for(i=0; i<player1->number_of_cards; i++) {
        print_card(buff_stream, player1->other_cards[i]);
    }
    fputs("\n", buff_stream);
    fprintf(buff_stream, "Your sum: %d.\n", calculate_sum(player1));

    fprintf(buff_stream, "Other player has %d cards.\n", player2->number_of_cards);
    if(!player1->stays)
        fprintf(buff_stream, "Press \"S\" to STAY or any other key to HIT \n");
    else
        fprintf(buff_stream, "Press any key... \n");

    fclose(buff_stream);
}

void final_message(struct player *player1, struct player *player2, void *buff, size_t buff_size) {
    FILE *buff_stream;
    int i;
    buff_stream = fmemopen(buff, buff_size, "w");
    fprintf(buff_stream, "++\nYour score: %d.\n", player1->score);
    fprintf(buff_stream, "Your %d cards:\n", player1->number_of_cards);
    for(i=0; i<player1->number_of_cards; i++) {
        print_card(buff_stream, player1->other_cards[i]);
    }
    fprintf(buff_stream, "\n");
    fprintf(buff_stream, "Your sum: %d.\n", calculate_sum(player1));
    fprintf(buff_stream, "Other player's score: %d.\n", player2->score);
    fprintf(buff_stream, "Other player's %d cards:\n", player2->number_of_cards);
    for(i=0; i<player2->number_of_cards; i++) {
        print_card(buff_stream, player2->other_cards[i]);
    }
    fprintf(buff_stream, "\n");
    fprintf(buff_stream, "Other player's sum: %d.\n", calculate_sum(player2));
    fprintf(buff_stream, "Press any key... ");

    fclose(buff_stream);
}


void print_card(FILE *buff_stream, int number) {
    char *kind_name;
    int kind = number % 4;
    int card = number / 4;

    char spades[] = "Spades";
    char hearts[] = "Hearts";
    char clubs[] = "Clubs";
    char diamonds[] = "Diamonds";


    switch(kind) {
    case 0:
        kind_name = spades;
        break;
    case 1:
        kind_name = hearts;
        break;
    case 2:
        kind_name = clubs;
        break;
    case 3:
        kind_name = diamonds;
        break;
    }

    switch(card) {
    case 0:
        fprintf(buff_stream, "A of %s, ", kind_name);
        break;
    case 10:
        fprintf(buff_stream, "J of %s, ", kind_name);
        break;

    case 11:
        fprintf(buff_stream, "Q of %s, ", kind_name);
        break;
    case 12:
        fprintf(buff_stream, "K of %s, ", kind_name);
        break;
    default:
        fprintf(buff_stream, "%d of %s, ", card+1, kind_name);
        break;
    }
}

void test_player_stats(void) {
    struct player player1, player2;
    char buffer[256];
    player1.aces = 0;
    player1.number_of_cards = 5;
    player1.other_cards[0] = 13;
    player1.other_cards[1] = 49;
    player1.other_cards[2] = 26;
    player1.other_cards[3] = 31;
    player1.other_cards[4] = 24;
    player1.score = 9000;
    player2.score = 666;
    player2.number_of_cards = 99;
    players_stats(&player1, &player2, buffer, sizeof buffer);
    puts(buffer);
}

int draw_card(int deck[], int *used_cards, struct player *some_player) {
    int card;
    int card_number;
    if(*used_cards >= NUMBER_OF_CARDS)
        return -1;
    if(some_player->number_of_cards >= MAX_IN_A_HAND)
        return -2;
    card = deck[*used_cards];
    card_number = some_player->number_of_cards;
    some_player->other_cards[card_number] = card;
    if ((card-1)/4 == 0)
        some_player->aces = 1;
    (*used_cards)++;
    (some_player->number_of_cards)++;
    return 0;
}

int calculate_sum(struct player *some_player) {
    int i;
    int sum=0;
    int card;
    for(i=0; i < some_player->number_of_cards; i++) {
        card = some_player->other_cards[i];
        card = card / 4;
        switch(card) {
        case 0:
            sum += 1; /* Ace */
            break;
        case 10:
        case 11:
        case 12:
            sum += 10; /* Royal cards */
            break;
        default:
            sum += card + 1; /* other */
            break;
        }
    }
    if((sum <= 11) && some_player->aces)
        sum += 10;
    return sum;
}
