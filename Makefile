all: server client

server: server.c shuffle.h shuffle.c game.h game.c
	$(CC) server.c shuffle.c game.c -o server --pedantic -Wall

client: client.c
	$(CC) client.c -o client --pedantic -Wall
clean:
	$(RM) server client *.o
