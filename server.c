#include "shuffle.h"
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include "game.h"

#define BACKLOG 10

void sigchld_handler(int s) {
    while(waitpid(-1, NULL, WNOHANG) > 0);
}

int main(int argc, char *argv[]) {
    int status;
    struct addrinfo hints, *servinfo, *p;
    int listenfd, client1fd, client2fd;
    socklen_t sin_size;
    const int yes=1;
    struct sockaddr_storage their_addr;
    char s[INET6_ADDRSTRLEN];
    struct sigaction sa;

    if(argc != 2) {
        fprintf(stderr, "Usage:\n"
                "%s port\n", argv[0]);
	test_player_stats();
        return 1;
    }

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if((status = getaddrinfo("0.0.0.0", argv[1], &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return 1;
    }

    /*   loop through all the results and bind to the first we can */
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((listenfd = socket(p->ai_family, p->ai_socktype,
                               p->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }

        if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &yes,
                       sizeof(int)) == -1) {
            perror("setsockopt");
            exit(1);
        }

        if (bind(listenfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(listenfd);
            perror("server: bind");
            continue;
        }

        break;
    }

    if (p == NULL)  {
        fprintf(stderr, "server: failed to bind\n");
        return 2;
    }

    freeaddrinfo(servinfo);

    sa.sa_handler = sigchld_handler; /* reap all dead processes */
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("sigaction");
        exit(1);
    }

    if (listen(listenfd, BACKLOG) == -1) {
        perror("listen");
        exit(1);
    }

    printf("server: waiting for connections...\n");

    while(1) {  /* main accept() loop */
        sin_size = sizeof their_addr;
        client1fd = accept(listenfd, (struct sockaddr *)&their_addr, &sin_size);
        if (client1fd == -1) {
            perror("accept");
            exit(1);
        }

        inet_ntop(their_addr.ss_family,
                  &(((struct sockaddr_in *)&their_addr)->sin_addr),
                  s, sizeof s);
        printf("server: got connection from %s (client1)\n", s);


        client2fd = accept(listenfd, (struct sockaddr *)&their_addr, &sin_size);
        if (client2fd == -1) {
            perror("accept");
            exit(1);
        }

        inet_ntop(their_addr.ss_family,
                  &(((struct sockaddr_in *)&their_addr)->sin_addr),
                  s, sizeof s);
        printf("server: got connection from %s (client2)\n", s);



        if (!fork()) { /* this is the child process */
            close(listenfd); /* child doesn't need the listener */
            game_loop(client1fd, client2fd);
            close(client1fd);
            close(client2fd);
            exit(0);
        }
        close(client1fd);  /* parent doesn't need this */
        close(client2fd);
    }
    close(listenfd);

    return 0;
}
